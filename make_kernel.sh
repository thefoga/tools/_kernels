# usage: `installKer /home/stefano/learnable-triangulation-pytorch`
function installKer {
    env_folder=$1
    ker_name=$(basename ${env_folder})  # will be name of kernel

    source ${env_folder}/.venv/bin/activate && which python  # just to see

    pip install ipykernel
    python -m ipykernel install --user --name=${ker_name}
}


# usage: `createKer /home/stefano/learnable-triangulation-pytorch`
function createKer {
    env_folder=$1
    k_inner=.venv  # name of virtualenv in folder

    cd ${env_folder}

    virtualenv -p /usr/bin/python3 ${k_inner}  # create virtual env
    source ${k_inner}/bin/activate && which python  # just to see

    pip install -r requirements.txt  # install requirements
    deactivate

    installKer ${env_folder}
}
